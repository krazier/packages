# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=weechat
pkgver=2.8
pkgrel=0
pkgdesc="Fast, light, extensible ncurses-based chat client"
url="https://www.weechat.org"
arch="all"
options="!check"  # requires all plugins be built.
license="GPL-3.0+"
depends=""
depends_dev="cmake aspell-dev curl-dev gnutls-dev libgcrypt-dev lua5.3-dev
	ncurses-dev perl-dev python3-dev ruby-dev tcl-dev zlib-dev guile-dev
	tk-dev"
checkdepends="cpputest"
makedepends="$depends_dev asciidoctor"
subpackages="$pkgname-dev $pkgname-spell:_plugin $pkgname-lua:_plugin
	$pkgname-perl:_plugin $pkgname-python:_plugin $pkgname-ruby:_plugin
	$pkgname-tcl:_plugin $pkgname-guile:_plugin $pkgname-doc"
source="https://www.weechat.org/files/src/$pkgname-$pkgver.tar.gz"

# secfixes:
#   1.7.1-r0:
#     - CVE-2017-8073
#   1.9.1-r0:
#     - CVE-2017-14727
#   2.7.1-r0:
#     - CVE-2020-8955

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_JAVASCRIPT=OFF \
		-DENABLE_MAN=ON \
		-DENABLE_NLS=OFF \
		-DENABLE_PHP=OFF \
		-Bbuild .
	make -C build
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir/" -C build install
}

_plugin() {
	_name="${subpkgname#*-}"
	_dir=usr/lib/weechat/plugins
	pkgdesc="WeeChat $_name plugin"
	depends="weechat"
	if [ "$_name" = spell ]; then
		provides="$pkgname-aspell=$pkgver-r$pkgrel"
	fi

	mkdir -p "$subpkgdir"/$_dir
	mv "$pkgdir"/$_dir/${_name}.so "$subpkgdir"/$_dir
}

sha512sums="3071fc6c5d88d4e388fc22f23242cf264b9533b389668914fc25e71e9939b739ba63a4e182445222ed0a7470dc0b1d958828b56d2c82ac47e9dfce6513c70d80  weechat-2.8.tar.gz"
