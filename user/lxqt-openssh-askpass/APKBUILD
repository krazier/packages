# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=lxqt-openssh-askpass
pkgver=0.15.0
_lxqt_build=0.7.0
pkgrel=0
pkgdesc="Graphical LXQt utility for inputting passwords for SSH agents"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev lxqt-build-tools>=$_lxqt_build
	liblxqt-dev>=${pkgver%.*}.0 kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxde/lxqt-openssh-askpass/releases/download/$pkgver/lxqt-openssh-askpass-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="e9dd41dd75892be234e6381670fa0b836bf040b00b7c59c7f23cadc9a916e8c646c4d44645a49183a305c6b755a01e6b8e6a3c0f84aa7c374cdb3ddd577e2f44  lxqt-openssh-askpass-0.15.0.tar.xz"
