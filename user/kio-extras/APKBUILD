# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kio-extras
pkgver=20.04.2
pkgrel=0
pkgdesc="KIO plugins for various data tasks"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires fully running KDE system to test.
license="LGPL-2.1+ AND (GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND GPL-2.0-only AND LGPL-2.0-only AND (LGPL-2.1+ OR GPL-2.0+) AND BSD-2-Clause AND LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev karchive-dev kconfig-dev kio-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev
	kiconthemes-dev ki18n-dev solid-dev kbookmarks-dev kguiaddons-dev
	kdnssd-dev kpty-dev kactivities-dev phonon-dev libtirpc-dev
	taglib-dev libmtp-dev gperf khtml-dev syntax-highlighting-dev
	kactivities-stats-dev"
makedepends="$depends_dev cmake extra-cmake-modules shared-mime-info"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-extras-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -I/usr/include/tirpc" \
		-DCMAKE_C_FLAGS="$CFLAGS -I/usr/include/tirpc" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="50c895f678ca4b8b7b60e1b7dd4821fc5e665159a0b48cf5d1fbccef8ef83ef129e08cb0792cf2d4b243ccff021269ed47b9bfa4c4f8628cfa424a06071e16a1  kio-extras-20.04.2.tar.xz"
