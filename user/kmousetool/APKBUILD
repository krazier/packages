# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmousetool
pkgver=20.04.2
pkgrel=0
pkgdesc="Tool to assist with clicking the mouse button"
url="https://userbase.kde.org/KMouseTool"
arch="all"
license="LGPL-2.1-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	kdoctools-dev kiconthemes-dev knotifications-dev kxmlgui-dev phonon-dev
	libxtst-dev libxt-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmousetool-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="85f8b0c1e33062a7f26b6bc3822499bdfb82bf2c3cdef7cc9f8a1c2262d9f35f385b30250b50276677a296c52732ec6d053db2300a9d9057462d6b6fa57061b0  kmousetool-20.04.2.tar.xz"
