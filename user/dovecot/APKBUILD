# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Lukasz Jendrysik <scadu@yandex.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Lee Starnes <lee@canned-death.us>
pkgname=dovecot
pkgver=2.3.10.1
_pkgvermajor=2.3
pkgrel=0
_pigeonholever=0.5.9
_pigeonholevermajor=${_pigeonholever%.*}
pkgdesc="IMAP and POP3 server"
url="https://www.dovecot.org/"
arch="all"
options="libtool"
license="LGPL-2.0+"
depends="openssl"
pkgusers="dovecot dovenull"
pkggroups="dovecot dovenull"
makedepends="
	autoconf
	automake
	bzip2-dev
	clucene-dev
	expat-dev
	krb5-dev
	libcap-dev
	openssl-dev
	libtool
	linux-headers
	linux-pam-dev
	mariadb-dev
	openldap-dev
	postgresql-dev
	sqlite-dev
	zlib-dev
	libsodium-dev
	"
install="$pkgname.pre-install $pkgname.post-install $pkgname.post-upgrade"
subpackages="
	$pkgname-doc
	$pkgname-dev
	$pkgname-openrc
	$pkgname-lmtpd
	$pkgname-pop3d
	$pkgname-submissiond
	$pkgname-pigeonhole-plugin-ldap:_sieve_ldap
	$pkgname-pigeonhole-plugin:_sieve
	$pkgname-sql
	$pkgname-pgsql
	$pkgname-mysql
	$pkgname-sqlite
	$pkgname-gssapi
	$pkgname-ldap
	$pkgname-fts-solr:_fts_solr
	$pkgname-fts-lucene:_fts_lucene
	"
source="https://www.dovecot.org/releases/$_pkgvermajor/dovecot-$pkgver.tar.gz
	https://pigeonhole.dovecot.org/releases/$_pkgvermajor/$pkgname-$_pkgvermajor-pigeonhole-$_pigeonholever.tar.gz
	skip-iconv-check.patch
	split-protocols.patch
	default-config.patch
	ssl-paths.patch
	dovecot.logrotate
	dovecot.initd
	"
_builddir_pigeonhole="$srcdir/$pkgname-$_pkgvermajor-pigeonhole-$_pigeonholever"

# secfixes:
#   2.3.10.1-r0:
#     - CVE-2020-10957
#     - CVE-2020-10958
#     - CVE-2020-10967

_configure() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--localstatedir=/var \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static \
		"$@"
}

build() {
	_configure \
		--with-gssapi=plugin \
		--with-ldap=plugin \
		--with-sql=plugin \
		--with-pam \
		--with-mysql \
		--with-sqlite \
		--with-pgsql \
		--with-solr \
		--with-lucene \
		--with-ssl=openssl \
		--with-ssldir=/etc/ssl/dovecot \
		--with-rundir=/run/dovecot
	make

	# Build pigeonhole plugin
	cd "$_builddir_pigeonhole"
	_configure \
		--with-dovecot="$builddir" \
		--with-ldap=plugin
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	cd "$pkgdir"

	install -d ./etc/ssl/dovecot
	install -D -m 755 "$srcdir"/dovecot.initd ./etc/init.d/dovecot
	install -D -m 644 "$srcdir"/dovecot.logrotate ./etc/logrotate.d/dovecot

	# default config
	rm ./etc/dovecot/*
	rm ./usr/share/doc/dovecot/mkcert.sh
	mv ./usr/share/doc/dovecot/dovecot-openssl.cnf ./etc/dovecot/
	mv ./usr/share/doc/dovecot/example-config/dovecot* \
		./usr/share/doc/dovecot/example-config/conf.d \
		./etc/dovecot/
	rm -fr ./usr/share/doc/dovecot/example-config

	# Installing pigeonhole plugin.
	cd "$_builddir_pigeonhole"
	make install DESTDIR="$pkgdir"

	# Moving config in the correct place
	mv "$pkgdir"/usr/share/doc/dovecot/example-config/conf.d/* \
		"$pkgdir"/etc/dovecot/conf.d

	# Remove libtool archives. abuild doesn't remove them automatically even without options=libtool.
	find "$pkgdir" -name '*.la' | xargs rm -f
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/dovecot
	mv "$pkgdir"/usr/lib/dovecot/dovecot-config \
		"$subpkgdir"/usr/lib/dovecot/
}

lmtpd() {
	pkgdesc="$pkgdesc (LMTP server)"
	_protocol lmtp
}

pop3d() {
	pkgdesc="$pkgdesc (POP3 daemon)"
	_protocol pop3
}

submissiond() {
	pkgdesc="$pkgdesc (mail submission agent)"
	_protocol submission
}

_protocol() {
	depends="$pkgname=$pkgver-r$pkgrel"
	_name="$1"
	_protocolsd="$subpkgdir/usr/share/dovecot/protocols.d"

	cd "$pkgdir"
	_submv usr/libexec/dovecot/$_name*
	_submv etc/dovecot/conf.d/*-$_name.conf

	mkdir -p "$_protocolsd"
	echo "protocols = \$protocols $_name" \
		> "$_protocolsd"/${subpkgname#$pkgname-}.conf
}

_sieve() {
	pkgdesc="Sieve and managesieve plugin for Dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*sieve_extprograms*')
	_submv $(find usr/ -name '*sieve_imapsieve*')
	_submv $(find usr/ -name '*sieve*')
	_submv $(find usr/ -name '*pigeonhole*')
	_submv $(find etc/dovecot/ -name '*sieve*')
}

_sieve_ldap() {
	pkgdesc="Sieve and managesieve plugin for Dovecot (LDAP support)"
	depends="$pkgname-pigeonhole-plugin=$pkgver-r$pkgrel $pkgname-ldap=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_sieve_storage_ldap_*')
}

pgsql() {
	pkgdesc="PostgreSQL driver for Dovecot"
	depends="$pkgname-sql=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_pgsql*')
}

mysql() {
	pkgdesc="MySQL driver for Dovecot"
	depends="$pkgname-sql=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_mysql*')
}

sqlite() {
	pkgdesc="SQLite driver for Dovecot"
	depends="$pkgname-sql=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_sqlite*')
}

gssapi() {
	pkgdesc="GSSAPI auth plugin for Dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_gssapi*')
}

ldap() {
	pkgdesc="LDAP auth plugin for Dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*[_-]ldap*')
	_submv $(find etc/dovecot/ -name '*-ldap.conf*')
}

sql() {
	pkgdesc="SQL plugin for dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*-sql.*')
	_submv $(find etc/dovecot/ -name '*-sql.conf*')
}

_fts_solr() {
	pkgdesc="FTS-Solr plugin for dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*fts*solr*')
}

_fts_lucene() {
	pkgdesc="FTS-Lucene plugin for dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*fts*lucene*')
}

_submv() {
	while [ $# -gt 0 ]; do
		_dir=${1%/*}
		mkdir -p "$subpkgdir"/$_dir
		mv "$pkgdir/$1" "$subpkgdir/$_dir/"
		[ "$(ls -A $pkgdir/$_dir)" ] || rmdir "$pkgdir"/$_dir
		shift
	done
}

sha512sums="5c07436a3e861993f241caa2c60f035c533c5fceb5c8540c1717d31bedd54b82299f7ea11bfee12c72d4d33985d93a7130c4f56877864a7ad21cf7373a29cc06  dovecot-2.3.10.1.tar.gz
1b8d2ac8d3985dde035fc45df519788a924ba971f3e39717f5196ea56a982d4156226586d0a964473525d086967883ea52f2e624e81f7035cb0952b76f2414d8  dovecot-2.3-pigeonhole-0.5.9.tar.gz
fe4fbeaedb377d809f105d9dbaf7c1b961aa99f246b77189a73b491dc1ae0aa9c68678dde90420ec53ec877c08f735b42d23edb13117d7268420e001aa30967a  skip-iconv-check.patch
794875dbf0ded1e82c5c3823660cf6996a7920079149cd8eed54231a53580d931b966dfb17185ab65e565e108545ecf6591bae82f935ab1b6ff65bb8ee93d7d5  split-protocols.patch
0d8f89c7ba6f884719b5f9fc89e8b2efbdc3e181de308abf9b1c1b0e42282f4df72c7bf62f574686967c10a8677356560c965713b9d146e2770aab17e95bcc07  default-config.patch
5e68a0042a7c11b3d8c411fc157f5960e2e3305dac11f4b6b880441e2b4105769ddf6c56f67a995af6e1a58f3bfa2c199ea51318a3a0e37c7ef0ae6c4109b13f  ssl-paths.patch
9f19698ab45969f1f94dc4bddf6de59317daee93c9421c81f2dbf8a7efe6acf89689f1d30f60f536737bb9526c315215d2bce694db27e7b8d7896036a59c31f0  dovecot.logrotate
d91951b81150d7a3ef6a674c0dc7b012f538164dac4b9d27a6801d31da6813b764995a438f69b6a680463e1b60a3b4f2959654f68e565fe116ea60312d5e5e70  dovecot.initd"
