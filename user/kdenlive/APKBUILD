# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdenlive
pkgver=20.04.2
pkgrel=0
pkgdesc="Libre video editor"
url="https://kdenlive.org/"
arch="all"
options="!check"  # keyframetest.cpp seems to be broken:
                  # it claims the KeyframeModel cannot be constructed,
		  # and then abort(3)s with an assertion in QAbstractItemModel
		  # I believe the fakeit/Mock code is subtly broken somewhere.
license="GPL-2.0-only"
depends="qt5-qtquickcontrols2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev kio-dev
	qt5-qtsvg-dev qt5-qtdeclarative-dev kxmlgui-dev karchive-dev kcrash-dev
	kbookmarks-dev kcoreaddons-dev kconfig-dev kconfigwidgets-dev mlt-dev
	kdbusaddons-dev kwidgetsaddons-dev knotifyconfig-dev knewstuff-dev
	knotifications-dev kguiaddons-dev ktextwidgets-dev kiconthemes-dev
	kdoctools-dev kfilemetadata-dev qt5-qtwebkit-dev v4l-utils-dev
	kdeclarative-dev qt5-qtmultimedia-dev rttr-dev qt5-qtquickcontrols2-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenlive-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f7bfc35121f5a27b01ab7c16b0911f90b758eb113bd55ca22783a5142d3df92493b7170c6bf31352aa44d4ca8d8a7fd12491d75f44a137551dfc531c4c1a5d10  kdenlive-20.04.2.tar.xz"
