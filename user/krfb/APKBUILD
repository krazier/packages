# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krfb
pkgver=20.04.2
pkgrel=0
pkgdesc="Share your screen with others for remote watching or assistance"
url="https://www.kde.org/applications/system/krfb/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake qt5-qtbase-dev qt5-qtx11extras-dev extra-cmake-modules
	ki18n-dev kcompletion-dev kconfig-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdnssd-dev kdoctools-dev knotifications-dev kwallet-dev
	kwidgetsaddons-dev kxmlgui-dev xcb-util-image-dev libvncserver-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/krfb-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3435f095faa9e01338fc2f8937ad81aa61efe177f4efe2bf22d40dc95df5e4c100bd6f822c7a6bf788a05a40c034cbe57c00c6c478f607d846ebdf50b2c47358  krfb-20.04.2.tar.xz"
