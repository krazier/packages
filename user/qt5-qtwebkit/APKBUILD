# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qtwebkit
_realname=$pkgname
#qtwebkit
pkgver=5.212.0_git20191114
_realver=$(printf '%s' "$pkgver" | sed 's/_/-/')
pkgrel=0
pkgdesc="Open source Web browser engine"
url="https://github.com/qtwebkit/qtwebkit"
arch="all"
options="!check"  # Tests fail (surprise), require X11.
license="LGPL-2.1+ AND BSD-3-Clause AND Others"
depends="gst-plugins-base"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev ninja sqlite-dev icu-dev ruby perl bison flex gperf
	libxml2-dev libxslt-dev libjpeg-turbo-dev libpng-dev zlib-dev glib-dev
	gstreamer-dev fontconfig-dev qt5-qtsensors-dev qt5-qtpositioning-dev
	qt5-qtdeclarative-dev qt5-qtwebchannel-dev libxcomposite-dev cmake
	libxrender-dev gst-plugins-base-dev hyphen-dev libexecinfo-dev
	ruby-dev glib-dev libgcrypt-dev libtasn1-dev"
subpackages="$pkgname-dev"
#source="https://github.com/qtwebkit/qtwebkit/releases/download/$_realname-$_realver/$_realname-$_realver.tar.xz
source="https://distfiles.adelielinux.org/source/$_realname-$_realver.tar.xz
	bug-931.patch
	cmake.patch
	ppc-llint.patch
	"
builddir="$srcdir"/$_realname-$_realver

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	# We can enable the JIT when it is stable on all Tier 1 platforms:
	# pmmx (ensure no SSE)
	# ppc
	# ppc64
	#
	# DONE:
	# aarch64
	# armv7
	# x86_64
	#
	# https://github.com/qtwebkit/qtwebkit/issues/930 
	#	-DENABLE_MEDIA_SOURCE=ON \
	#	-DENABLE_VIDEO=ON \
	#	-DENABLE_WEB_AUDIO=ON \
	#	-DUSE_GSTREAMER=ON \
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_JIT=OFF \
		-DENABLE_PRINT_SUPPORT=ON \
		-DENABLE_QT_WEBCHANNEL=ON \
		-DPORT=Qt \
		-DUSE_SYSTEM_MALLOC=ON \
		-DUSE_WOFF2=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ec009773953b9f3e2c0e47072cd678f7044b16219c85879a6031c109c7b2b84f426564bb673ccaafa0bcc3f7a656e3cb0d8e3be28d3bbfb401adcd1a6b8b9edd  qt5-qtwebkit-5.212.0-git20191114.tar.xz
85adb979eb5d58ccab85d07d6a6db470b34bdb6fd8af2eef04ce268da41a596b4a19295c97dca1201daf4a51ca9e183bb27dd36b9b12b0e149224793d1c190a9  bug-931.patch
4ee26787bf01f3067fae51e1942b72d783629b13fe13ffcfba9329bbea823de7b751905f01c71c6c498164cb41a33765debce375a034dde83e3f784d6e549ada  cmake.patch
0b358e4df16e8792c995a385386bfe9e197e23854f30129b504d3ba851cdfab3a2adef462a6ae3a775ec01d02cd67ef404259491339336c6ce896d7940597c95  ppc-llint.patch"
