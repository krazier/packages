# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gwenview
pkgver=20.04.2
pkgrel=0
pkgdesc="Fast and easy image viewer by KDE"
url="https://www.kde.org/applications/graphics/gwenview/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev phonon-dev
	kio-dev kactivities-dev kitemmodels-dev ki18n-dev kdoctools-dev
	kparts-dev kwindowsystem-dev kiconthemes-dev knotifications-dev
	libkipi-dev libjpeg-turbo-dev libpng-dev lcms2-dev zlib-dev exiv2-dev
	baloo-dev libkdcraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/gwenview-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	LDFLAGS="$LDFLAGS -Wl,-z,stack-size=1048576" cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="28e152dd353273a19c31e5fb324e6ed09586a70ca96a5e4071ef70d8afc795eaad1b8907519acc4144b11d69eb88afb155e8585cbb59d0330560c6525e3d3baf  gwenview-20.04.2.tar.xz"
