# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=fish
pkgver=3.0.2
pkgrel=0
pkgdesc="Modern interactive commandline shell"
url="http://www.fishshell.com"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause AND BSD-2-Clause AND GPL-2.0+ AND GPL-2.0-only AND ISC"
depends="bc"
depends_dev="$pkgname-tools"
makedepends="libtool doxygen ncurses-dev pcre2-dev"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-tools::noarch"
source="https://github.com/fish-shell/fish-shell/releases/download/$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make install DESTDIR="$pkgdir"
	rm -fr "$pkgdir"/usr/share/$pkgname/groff
}

dev() {
	default_dev

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/pkgconfig "$subpkgdir"/usr/share
}

doc() {
	default_doc

	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/man "$subpkgdir"/usr/share/$pkgname
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	depends="$pkgname python3"

	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/tools "$subpkgdir"/usr/share/$pkgname
}

sha512sums="cd4a8e6a0d7770ef17e92d3d20ea23e754b9fa53e5ee5459ab5838fcbbcac69544ca2f83551e93a004b140cc14ec556860a711ce216197753c1704901518c9c3  fish-3.0.2.tar.gz"
