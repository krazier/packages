# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libjpeg-turbo
pkgver=2.0.4
pkgrel=1
pkgdesc="Accelerated JPEG compression and decompression library"
url="https://libjpeg-turbo.org/"
arch="all"
license="IJG AND BSD-3-Clause AND Zlib"
depends=""
makedepends="cmake"
subpackages="$pkgname-doc $pkgname-dev $pkgname-utils"
source="https://downloads.sourceforge.net/libjpeg-turbo/libjpeg-turbo-$pkgver.tar.gz
	CVE-2020-13790.patch
	"

case "$CTARGET_ARCH" in
pmmx | x86 | x86_64)	makedepends="$makedepends nasm" ;;
esac

# secfixes:
#   2.0.3-r0:
#     - CVE-2019-2201
#   2.0.4-r1:
#     - CVE-2020-13790

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	# https://github.com/libjpeg-turbo/libjpeg-turbo/issues/344
	# https://github.com/libjpeg-turbo/libjpeg-turbo/issues/428
	case "$CARCH" in
	ppc) _floattest=64bit;;
	esac

	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DEFAULT_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_STATIC=OFF \
		-DWITH_JPEG8=ON \
		${_floattest:+-DFLOATTEST="$_floattest"} \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Utilities for manipulating JPEG images"
	replaces="jpeg"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="708c2e7418d9ed5abca313e2ff5a08f8176d79cad2127573cda6036583c201973db4cfb0eafc0fc8f57ecc7b000d2b4af95980de54de5a0aed45969e993a5bf9  libjpeg-turbo-2.0.4.tar.gz
83752558d0cf60508a9ccd55505b91f4faa22277537916629a045b2aaa0cb3649e2f90b0df26d389687dc4aba78bdf76e64fc5e5eb324a65026ec86cd95dbe6a  CVE-2020-13790.patch"
