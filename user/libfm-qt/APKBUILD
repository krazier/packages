# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=libfm-qt
pkgver=0.15.1
pkgrel=0
pkgdesc="Qt library for file management and bindings for libfm"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
depends_dev="libfm-dev menu-cache-dev libexif-dev"
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.7.0 qt5-qttools-dev
	qt5-qtx11extras-dev $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/libfm-qt/releases/download/$pkgver/libfm-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -O0" \
		-DCMAKE_C_FLAGS="$CFLAGS -O0" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="9bd3f61d830e4f7ba3e72d84e3ff1a07544f6f461d6eea7ecf2b31056020e08d47d83ef920b6e1a481a470dadfb995a6b3e3b57a13574d6abca617a84c71bdf5  libfm-qt-0.15.1.tar.xz"
