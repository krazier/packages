# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalc
pkgver=20.04.2
pkgrel=0
pkgdesc="Calculator with many mathematical, scientific, and logic functions"
url="https://utils.kde.org/projects/kcalc/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev ki18n-dev
	kconfigwidgets-dev kdoctools-dev kguiaddons-dev kinit-dev kxmlgui-dev
	knotifications-dev gmp-dev mpfr-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalc-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="33c92a0fa652a9351c8a265000203417aa398aeb1b5b29d8fdc7029a705956d6d495c1317c09f782c2978efb75f68b1ba1bd2c816818d868a96e8958491a8ee3  kcalc-20.04.2.tar.xz"
