# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=konsole
pkgver=20.04.2
pkgrel=0
pkgdesc="Terminal emulator for Qt/KDE"
url="https://konsole.kde.org/"
arch="all"
options="!check"  # Requires running DBus session bus.
license="GPL-2.0-only AND LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kbookmarks-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kguiaddons-dev kdbusaddons-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev knotifications-dev knotifyconfig-dev kparts-dev
	kpty-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev python3
	kwindowsystem-dev kxmlgui-dev kdbusaddons-dev knewstuff-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/konsole-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0f998c6f69bac905385b51c2d2af3b4ed1e835e1a33208c59238e3950c6c5cf3580a435ea5acacfbd031eecf2308e2c96e9df30f1ccf44154a2a637ca073906d  konsole-20.04.2.tar.xz"
