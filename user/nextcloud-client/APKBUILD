# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=nextcloud-client
pkgver=2.6.3
pkgrel=0
pkgdesc="Nextcloud desktop client"
url="https://github.com/nextcloud/desktop"
arch="all"
license="GPL-2.0+ AND LGPL-2.1+ AND Public-Domain AND MIT AND (Custom:Digia-Qt OR LGPL-2.1-only WITH Qt-LGPL-exception-1.1) AND (Custom:Digia-Qt OR LGPL-2.1-only WITH Qt-LGPL-exception-1.1 OR GPL-3.0-only)"
depends=""
makedepends="cmake qt5-qttools-dev qtkeychain-dev zlib-dev
	openssl-dev sqlite-dev qt5-qtsvg-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/nextcloud/desktop/archive/v$pkgver.tar.gz
	no-webengine.patch
	test-fix-include.patch"
builddir="$srcdir/desktop-$pkgver"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWITH_CRASHREPORTER=bool:OFF \
		-DUNIT_TESTING=bool:ON \
		-DNO_SHIBBOLETH=bool:ON \
		-DNO_WEBENGINE=bool:ON .
	make
}

check() {
	# ChunkingTestNg fails sporadically when run with the other tests based
	# on a timeout, but runs fine on its own
	make test || make test ARGS="--rerun-failed --output-on-failure"
}

package() {
	make DESTDIR="$pkgdir" install
	cat > "$pkgdir"/etc/Nextcloud/Nextcloud.conf <<-EOF
		[General]
		skipUpdateCheck=true
	EOF
}

sha512sums="ddeb8a32e0208bc415bf421cb3ce05713adb617fa78c0b5cb4d894c9e8d3499dcc495c115de3ea7abb0c402eef4bc64ede8c59a7f056acda47779096fedc8025  nextcloud-client-2.6.3.tar.gz
38dd89ca2bf67294187bb4d5c59cdf725ec3b502f23bf4a60210e85c66f6a7e00da0e0b41775bc138159bed300dc60a1f179d1d6ad3b7a27e1aadf4f0576f348  no-webengine.patch
c14054e6dc228ca77a659371b3c151e0551477fce3a2678a54da4ba73adbcf972a78114c5649adc3dc813fb01b55bbb812620caac0bc0460519424c2caa6c81f  test-fix-include.patch"
