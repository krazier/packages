# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpat
pkgver=20.04.2
pkgrel=0
pkgdesc="Collection of card games for KDE"
url="https://games.kde.org/game.php?game=kpat"
arch="all"
options="!check"  # Requires running X11
license="GPL-2.0-only"
depends="libkdegames-carddecks"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev
	kdbusaddons-dev kdoctools-dev kguiaddons-dev kio-dev knewstuff-dev
	kwidgetsaddons-dev kxmlgui-dev libkdegames-dev shared-mime-info
	freecell-solver-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpat-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="60ebb6b4278a9dd91d34ab17b04d8adb1d4c48039527b0b86df8cfe91bcadc517462ebcbdcfb536946db3fb666be837cd31f2540ef81ed061e939d7b720485d5  kpat-20.04.2.tar.xz"
