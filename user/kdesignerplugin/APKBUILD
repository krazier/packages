# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesignerplugin
pkgver=5.71.0
pkgrel=0
pkgdesc="Qt Designer plugin for KDE widgets"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 display.
license="LGPL-2.1-only"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev kio-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kplotting-dev
	ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev sonnet-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdesignerplugin-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="550de8c6c9b204968246aa4a99bf51e9c2c20d1b882c5f402085e8929e5a4df53710cdea356f201a3666253358f5fe98cc17c0d69334d51904291193045de7ac  kdesignerplugin-5.71.0.tar.xz"
