# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knewstuff
pkgver=5.71.0
pkgrel=0
pkgdesc="Framework for discovering and downloading plugins, themes, and more"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends="kirigami2"
depends_dev="qt5-qtbase-dev attica-dev kconfig-dev kservice-dev kxmlgui-dev
	openssl-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev karchive-dev kcompletion-dev kcoreaddons-dev kio-dev
	ki18n-dev kiconthemes-dev kirigami2-dev kitemviews-dev kpackage-dev
	ktextwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knewstuff-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# kmoretoolstest requires X11.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'kmoretoolstest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0b5a65086d0ca08ac3288523262762e70a03a208400068409cd1c964bb805f1328a7314692164d9ab70b3e07d3438f291b8d18bca99bbdd134a3f7ea658e5297  knewstuff-5.71.0.tar.xz"
