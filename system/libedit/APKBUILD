# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libedit
pkgver=20191231.3.1
_realver="${pkgver%%.*}-${pkgver#*.}"
pkgrel=0
pkgdesc="Library providing line editing, history, and tokenisation functions"
url="https://thrysoee.dk/editline/"
arch="all"
license="BSD-2-Clause"
depends=""
depends_dev="ncurses-dev"  # XXX is this always unconditionally needed?
makedepends="$depends_dev autoconf automake libtool"
subpackages="$pkgname-dev $pkgname-doc"
source="https://thrysoee.dk/editline/libedit-$_realver.tar.gz"
builddir="$srcdir/$pkgname-$_realver"

prepare() {
	default_prepare
	autoreconf -v -f --install
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1df2eced98e8db1bb0af940678c154d87e3b11dd21e65a903682367f5feace5112f9a543b8e0cb04bbfeaaf73729f808db2d9c302637fc063e81c0a37777ac2c  libedit-20191231-3.1.tar.gz"
