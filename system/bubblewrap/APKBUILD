# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=bubblewrap
pkgver=0.4.1
pkgrel=0
pkgdesc="Unprivileged sandboxing tool"
url="https://github.com/projectatomic/bubblewrap"
arch="all"
options="!check suid"  # requires suid to already be set in order to check
license="LGPL-2.0+"
makedepends="autoconf automake libcap-dev docbook-xsl"
checkdepends="python3 sudo"
subpackages="$pkgname-nosuid $pkgname-doc
	$pkgname-bash-completion:bashcomp:noarch"
source="bubblewrap-$pkgver.tar.gz::https://github.com/containers/bubblewrap/archive/v$pkgver.tar.gz
	realpath-workaround.patch
	tests.patch
	"

# secfixes:
#   0.3.3-r0:
#     - CVE-2019-12439
#   0.4.1-r0:
#     - GHSA-j2qp-rvxj-43vj

prepare() {
	default_prepare
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-priv-mode=setuid
	make
}

check() {
	# 1. chown root and chmod u+s $builddir/test-bwrap
	# 2. Run abuild check (suid test)
	# 3. Unset permissions on test-bwrap
	# 4. Run abuild check again (nosuid test)
	#
	# As of 0.4.1, all tests pass except those relating to bind mounts
	# over symlinks. Those tests fail because musl's realpath depends on
	# the availability of /proc, which is not available in the middle of
	# the setup procedure since pivot_root has been performed at least
	# once. They have been patched to be skipped.
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

nosuid() {
	pkgdesc="$pkgdesc (non-setuid binary)"

	mkdir -p "$subpkgdir"/usr/bin
	cp "$pkgdir"/usr/bin/bwrap \
		"$subpkgdir"/usr/bin/bwrap.nosuid
	chmod -s "$subpkgdir"/usr/bin/bwrap.nosuid
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion/ "$subpkgdir"/usr/share/
}

sha512sums="83e036e242503e1364b2d0052bba5127175891203c57bd22ba47a1b1e934fdca64ca620cd0e48c903fa2bc7cdcf92339b8a7fcb8716b54c2e28034b6d6f86adc  bubblewrap-0.4.1.tar.gz
400a0446670ebf80f16739f1a7a2878aadc3099424f957ba09ec3df780506c23a11368f0578c9e352d7ca6473fa713df826fad7a20c50338aa5f9fa9ac6b84a4  realpath-workaround.patch
d572a6296729ab192dd4f04707e0271df600d565897ce089b7f00b9ae6c62e71a087e864b4c4972e0a64aeb222a337ff4ed95560620c200cc44534db1ca79efd  tests.patch"
