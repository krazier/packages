# Contributor: William Pitcock <nenolod@dereferenced.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=shadow
pkgver=4.7
pkgrel=0
pkgdesc="Login and password management utilities"
url="https://github.com/shadow-maint/shadow"
arch="all"
options="suid"
license="BSD-3-Clause OR Artistic-1.0-Perl"
depends=""
makedepends="linux-pam-dev utmps-dev"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-lang $pkgname-uidmap"
source="https://github.com/shadow-maint/shadow/releases/download/$pkgver/shadow-$pkgver.tar.xz
	login.pamd
	dots-in-usernames.patch
	useradd-usergroups.patch
	pam-useradd.patch
	useradd-zsh.patch
	"
# secfixes:
#   4.5-r0:
#   - CVE-2017-12424
#   4.2.1-r11:
#   - CVE-2017-2616
#   4.2.1-r7:
#   - CVE-2016-6252

build() {
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-utmpx \
		--with-libpam \
		--without-audit \
		--without-selinux \
		--without-acl \
		--without-attr \
		--without-tcb \
		--without-nscd \
		--without-group-name-max-length
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# Do not install these pam.d files they are broken and outdated.
	rm "$pkgdir"/etc/pam.d/*

	# install some pam.d files based on a patched useradd
	for pamf in groupadd groupdel groupmems groupmod \
		 useradd userdel usermod
	do
		install -m0644 etc/pam.d/useradd \
			"$pkgdir/etc/pam.d/$pamf"
	done
	# nologin is provided by util-linux.
	rm "$pkgdir"/sbin/nologin

	# However, install our own for login.
	cp "$srcdir"/login.pamd "$pkgdir"/etc/pam.d/login

	# /etc/login.defs is not very useful - replace it with an *almost* blank file.
	rm "$pkgdir"/etc/login.defs
	echo "USERGROUPS_ENAB yes" > "$pkgdir"/etc/login.defs

	# Avoid conflict with man-pages.
	rm "$pkgdir"/usr/share/man/man3/getspnam.3* \
		"$pkgdir"/usr/share/man/man5/passwd.5* \
		"$pkgdir"/usr/share/man/man1/groups.1* \
		"$pkgdir"/usr/share/man/man8/nologin.8*
}

uidmap() {
	pkgdesc="Utilities for using subordinate UIDs and GIDs"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/new*idmap "$subpkgdir"/usr/bin/
	chmod 4711 "$subpkgdir"/usr/bin/new*idmap

	# Used e.g. for unprivileged LXC containers.
	mkdir "$subpkgdir"/etc
	touch "$subpkgdir"/etc/subuid "$subpkgdir"/etc/subgid
}

sha512sums="331ff09ec084149b88ac23c0f7e3b3eb02deba03a1623d46f0abdab4dbae50db2146c3cab8c1e36e8f0b496089204d2cc274265f552fb81bc2deb49ce44c7284  shadow-4.7.tar.xz
46a6f83f3698e101b58b8682852da749619412f75dfa85cecad03d0847f6c3dc452d984510db7094220e4570a0565b83b0556e16198ad894a3ec84b3e513d58d  login.pamd
e5b276be0852c7c50257e73237144141991b628c2032e47e066d3ae1f8a480d4aff9ccb3f29a9ee25cb3fd39a1f6e1f8349be4ace5e1db7c392b3c3dd3a08845  dots-in-usernames.patch
49f1d5ded82d2d479805c77d7cc6274c30233596e375b28306b31a33f8fbfc3611dbc77d606081b8300247908c267297dbb6c5d1a30d56095dda53c6a636fb56  useradd-usergroups.patch
0b4587e263cb6be12fa5ae6bc3b3fc4d3696dae355bc67d085dc58c52ff96edb4d163b95db2092b8c2f3310839430cac03c7af356641b42e24ee4aa6410f5cf1  pam-useradd.patch
7f8e19cdf0b6dfb111feedd528f31aa06e79450c5bb35549ba3ebabede2661f65833adf164237fced77780d1254a66a3595b6b1c05571695e322514f549f3cc7  useradd-zsh.patch"
